import Vue from 'vue';
import * as VueGoogleMaps from 'vue2-google-maps';
import App from './App.vue';
import router from './router';
import store from './store';

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyDi81FTiLpb9-JBYH7Exkq-MSRFs3T7yG4',
  },
  installComponents: true,
})

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
