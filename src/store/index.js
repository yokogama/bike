import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import router from '../router/index.js';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    meetings: [],
    // filteredMeetings: [],
    activeMeetingId: null,
    searchQuery: '',
    currentPage: 1,
    perPage: 1,
    nowDate: new Date(),
    limitDate: '2050.01.01',
    alertMsg: '',
    alertClass: '',
    GMKey: 'AIzaSyDi81FTiLpb9-JBYH7Exkq-MSRFs3T7yG4',
  },
  getters: {
    filteredMeetings: (state) => state.meetings.filter((meeting) => meeting.title.toLowerCase().includes(state.searchQuery.toLowerCase())),
    meetingsOnPage: (state, getters) => {
      const start = (state.currentPage - 1) * state.perPage;
      const end = start + state.perPage;
      const sortedMeetings = getters.filteredMeetings.sort((a, b) => {
        if (a.date === b.date) {
          if (a.time === b.time) {
            return a.title > b.title ? 1 : -1;
          }
          return a.time > b.time ? 1 : -1;
        }
        return a.date > b.date ? 1 : -1;
      });
      return sortedMeetings.slice(Math.max(start, 0), Math.min(end, sortedMeetings.length));
    },
    pages: (state, getters) => Math.ceil(getters.filteredMeetings.length / state.perPage),
    activeMeeting: (state) => state.meetings.find((meeting) => meeting.id === state.activeMeetingId),
    actualDate: (state) => {
      const year = state.nowDate.getFullYear();
      const month = state.nowDate.getMonth() < 10 ? `0${state.nowDate.getMonth() + 1}` : state.nowDate.getMonth();
      const day = state.nowDate.getDate() < 10 ? `0${state.nowDate.getDate()}` : state.nowDate.getDate();
      const actualDate = `${year}-${month}-${day}`;
      return actualDate;
    },
    actualTime: (state) => {
      const minutes = state.nowDate.getMinutes() < 10 ? `0${state.nowDate.getMinutes()}` : state.nowDate.getMinutes();
      const hours = state.nowDate.getHours() < 10 ? `0${state.nowDate.getHours()}` : state.nowDate.getHours();
      const actualTime = `${hours}:${minutes}`;
      return actualTime;
    },
  },
  mutations: {
    setAlert(state, alert) {
      state.alertMsg = alert.msg;
      state.alertClass = alert.className;
    },
    unsetAlert(state) {
      state.alertMsg = '';
      state.alertClass = '';
    },
    changePage(state, page) {
      state.currentPage = page;
    },
    setFirstPage(state) {
      state.currentPage = 1;
    },
    findMeeting: (state, query) => {
      state.searchQuery = query;
    },
    setMeetings: (state, meetings) => {
      state.meetings = meetings;
    },
    // setFilteredMeetings: (state, meetings) => {
    //   state.filteredMeetings = meetings;
    // },
    newMeeting: (state, meeting) => {
      state.meetings.unshift(meeting);
    },
    setActiveMeetingId: (state, meetingId) => {
      state.activeMeetingId = meetingId;
      state.searchQuery = '';
    },
    clearActiveMeetingId: (state) => {
      state.activeMeetingId = null;
    },
    removeMeeting: (state, id) => {
      state.meetings = state.meetings.filter((meeting) => meeting.id !== id)
    },
    updateMeeting: (state, editedMeeting) => {
      const index = state.meetings.find((meeting) => meeting.id === editedMeeting.id)
      if (index !== -1) {
        state.meetings.splice(index, 1, editedMeeting);
      }
    },
  },
  actions: {
    async fetchMeetings({ commit, getters }) {
      const response = await axios.get(`http://localhost:1337/meetings?date_gte=${getters.actualDate}`);
      commit('setMeetings', response.data);
      // commit('setFilteredMeetings', response.data);
    },
    async addMeeting({ commit, state }, formData) {
      await axios.get(`https://maps.googleapis.com/maps/api/geocode/json?address=${formData.localization}&key=${state.GMKey}`)
        .then((data) => {
          Object.assign(formData.position,
            {
              lat: data.data.results[0].geometry.location.lat,
              lng: data.data.results[0].geometry.location.lng,
            });
        })
      const response = await axios.post('http://localhost:1337/meetings', formData);
      commit('newMeeting', response.data)
      commit('setFirstPage')
      await router.push('/');
    },
    async deleteMeeting({ commit }, id) {
      await axios.delete(`http://localhost:1337/meetings/${id}`);
      commit('removeMeeting', id)
      commit('clearActiveMeetingId', id)
      // commit('setFirstPage')
    },
    async editMeeting({ state, commit }, formData) {
      await axios.get(`https://maps.googleapis.com/maps/api/geocode/json?address=${formData.localization}&key=${state.GMKey}`)
        .then((data) => {
          Object.assign(formData.position,
            {
              lat: data.data.results[0].geometry.location.lat,
              lng: data.data.results[0].geometry.location.lng,
            });
        })
      const response = await axios.put(`http://localhost:1337/meetings/${this.state.activeMeetingId}`, formData);
      commit('updateMeeting', response.data)
      // commit('setFirstPage')
      await router.push('/');
    },
    showAlert({ commit }, alert) {
      commit('setAlert', alert);
      setTimeout(() => {
        commit('unsetAlert')
      }, 2000);
    },
  },
});
