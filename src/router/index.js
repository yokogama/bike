import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '@/views/Home.vue';
import AddView from '@/views/AddView.vue';
import EditView from '../views/EditView.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
  },
  {
    path: '/add',
    name: 'AddView',
    component: AddView,
  },
  {
    path: '/edit',
    name: 'EditView',
    component: EditView,
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
